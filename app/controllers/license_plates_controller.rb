class LicensePlatesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  include LicensePlatesHelper

  def index
    @results = LprResult.where(control_car: params[:car_nr]).order(created_at: :desc)
  end

  def plate_scanned
    begin
      plate_info = get_plate_info
      lpr_result = save_lpr_result(plate_info)
      publish_lpr_result(lpr_result)
      render json: {result: true}.to_json
    rescue Exception => e

      render json: {result: false}.to_json
    end
  end


  def delete_all_results
    unless params[:car_nr].blank?
      LprResult.destroy_all(control_car: params[:car_nr].upcase)
    end
    redirect_to license_plates_path(car_nr: params[:car_nr].upcase)
  end

  private


end

class LprResult < ActiveRecord::Base
  has_attached_file :image
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def image_url
    self.image.url
  end

  def save_image?
   !self.allowed_to_park
  end
end

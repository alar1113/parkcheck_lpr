module LicensePlatesHelper
  def get_plate_info
    {
        plate: params[:plate],
        area: params[:area],
        control_car: params[:location],
        allowed_to_park: is_allowed_to_park(params[:plate])
    }
  end

  def is_allowed_to_park(license_plate)
    Random.rand > 0.5
  end


  def publish_lpr_result(lpr_result)
    request_body = {
        allowed_to_park: lpr_result.allowed_to_park,
        html: render_to_string(
            :partial => 'license_plates/lpr_result_row',
            :locals => {:result => lpr_result})
    }
    WebsocketRails[:control_car_results].trigger lpr_result.control_car, request_body
  end

  def save_lpr_result(plate_info)
    lpr_result = LprResult.new(plate_info)
    if lpr_result.save_image?
      lpr_result.image = params[:file]
    end
    lpr_result.save
    lpr_result
  end
end

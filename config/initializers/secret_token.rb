# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ParkcheckLpr::Application.config.secret_key_base = 'a3e4af27a66eb018b8e874cce16312fa6fc91bcde779526f7cde353866656c3b662f58ba7de02fad73ca653dc15b0598c14a944b1a247ccc6cfd8ee8e578215e'

class CreateLprResults < ActiveRecord::Migration
  def change
    create_table :lpr_results do |t|
      t.string :plate
      t.string :area
      t.string :control_car
      t.boolean :allowed_to_park
      t.integer :zone_login_id
      t.attachment :image
      t.timestamps
    end
    add_index :lpr_results, :plate
    add_index :lpr_results, :control_car
  end
end

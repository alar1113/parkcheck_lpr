# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150429143314) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "lpr_results", force: true do |t|
    t.string   "plate"
    t.string   "area"
    t.string   "control_car"
    t.boolean  "allowed_to_park"
    t.integer  "zone_login_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "lpr_results", ["control_car"], name: "index_lpr_results_on_control_car", using: :btree
  add_index "lpr_results", ["plate"], name: "index_lpr_results_on_plate", using: :btree

end
